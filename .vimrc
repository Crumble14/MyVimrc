syntax on

set ruler
set number

set tabstop=4
set shiftwidth=4
set smartindent

set hlsearch

set colorcolumn=81

inoremap ( ()<left>
inoremap [ []<left>
inoremap { {}<left>

let @c = 'aclass {public:	// TODOprivate:	// TODOOB$a;OD%OA$a'
let @e = 'aenum {// TODOOB$a;OD%OA$a'
let @n = 'anamespace {// TODOOB%OA$a'
let @s = 'astruct {// TODOOB$a;OD%OA$a'
let @u = 'ausing namespace ;OD'
